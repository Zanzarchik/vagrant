#!/bin/bash
export DISTR=/tmp/vagrant/distr

chmod 755 -R $DISTR

if [ -f "$DISTR/need_common_install" ]; then 
	sudo yum update -y
	sudo yum install wget unzip -y
	sudo yum install gcc kernel-devel make -y

fi

if [ -f "$DISTR/change_timezone" ]; then 
	$DISTR/change_timezone
fi

if [ -f "$DISTR/java_install/install" ]; then $DISTR/java_install/install
fi

if [ -f "$DISTR/samba_install/install" ]; then $DISTR/samba_install/install
fi

rm -rf /tmp/*